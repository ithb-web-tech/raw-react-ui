export default function isEmpty(value) {
  if (value === undefined || value === null) return true;
  if (typeof value === "string" && value.length === 0) return true;
  if (Array.isArray(value) && value.length === 0) return true;
  return false;
}
