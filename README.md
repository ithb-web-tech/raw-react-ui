# ITHB Web Tech UI

## Prerequisites

-   NodeJS
-   NPM

## Installation

1.  ```bash
    npm i
    ```

1.  Run the webapp in development mode.
    View it at http://localhost:3000 to view it.
    ```bash
    npm start
    ```

## Changing Enviornment settings

the environment setting is available in file `.env`. The `.env` file is committed in the git and it's advised for you to create the `.env.local`. The value will be cascading with the `.env.local` having higher priority than `.env`.

1. `cp .env .env.local`
1. edit the `.env.local` according to your need
